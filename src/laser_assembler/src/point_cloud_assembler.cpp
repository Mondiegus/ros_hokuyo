#include "laser_assembler/base_assembler.h"


using namespace std ;

namespace laser_assembler
{

/**
 * \brief Maintains a history of incremental point clouds (usually from laser scans) and generates a point cloud upon request
 * \todo Clean up the doxygen part of this header
 * params
 *  * (Several params are inherited from BaseAssemblerSrv)
 */
class PointCloudAssembler : public BaseAssembler<sensor_msgs::PointCloud>
{
public:
  PointCloudAssembler() : BaseAssembler<sensor_msgs::PointCloud>("max_clouds")
  {

  }

  ~PointCloudAssembler()
  {

  }

  unsigned int GetPointsInScan(const sensor_msgs::PointCloud& scan)
  {
    return (scan.points.size ());
  }

  void ConvertToCloud(const string& fixed_frame_id, const sensor_msgs::PointCloud& scan_in, sensor_msgs::PointCloud& cloud_out)
  {
    tf_->transformPointCloud(fixed_frame_id, scan_in, cloud_out) ;
    return ;
  }

private:

};

}

using namespace laser_assembler ;

int main(int argc, char **argv)
{
  ros::init(argc, argv, "point_cloud_assembler");
  PointCloudAssembler pc_assembler;
  pc_assembler.start("cloud");
  ros::spin();

  return 0;
}
