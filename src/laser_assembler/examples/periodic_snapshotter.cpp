

#include <fstream>
#include <cstdio>
#include <ros/ros.h>

// Services
#include "laser_assembler/AssembleScans.h"

// Messages
#include "sensor_msgs/PointCloud.h"


/***
 * This a simple test app that requests a point cloud from the
 * point_cloud_assembler every 4 seconds, and then publishes the
 * resulting data
 */
int flag2=1;
int flag3=0;
namespace laser_assembler
{

class PeriodicSnapshotter
{

public:

  PeriodicSnapshotter()
  {
    // Create a publisher for the clouds that we assemble
    pub_ = n_.advertise<sensor_msgs::PointCloud> ("assembled_cloud", 1);
    // Create the service client for calling the assembler
    client_ = n_.serviceClient<AssembleScans>("assemble_scans"); // szukaj w /laser_assembler/include/laser_assembler/base_assembler.h
    // Start the timer that will trigger the processing loop (timerCallback)
    timer_ = n_.createTimer(ros::Duration(0,0), &PeriodicSnapshotter::timerCallback, this);
    // Need to track if we've called the timerCallback at least once
    first_time_ = true;
  
  }

  void timerCallback(const ros::TimerEvent& e)
  {		
	ROS_INFO("Flag2: %d",flag2); 
	timer_.setPeriod(ros::Duration(30,0));	
	ROS_INFO("DUPA");

    // We don't want to build a cloud the first callback, since we we
    //   don't have a start and end time yet
    if (first_time_)
    {
      first_time_ = false;
      return;
    }

    // Populate our service request based on our timer callback times
    AssembleScans srv;
    srv.request.begin = e.last_real;
    srv.request.end   = e.current_real;
    // Make the service call
    if (client_.call(srv))//wywołujemy serwis "assemble_scans" i z base_assembler odpalamy funkcję w 273 linii, tutaj już tworzymy chmurę pubktów, gdzie wywołanie do samej obróbki skanów????
    {
	ROS_INFO("Published Cloud with %u points", (uint32_t)(srv.response.cloud.points.size())) ;
	pub_.publish(srv.response.cloud);
	
    }
    else
    {
      ROS_ERROR("Error making service call\n") ;
    }
	ROS_INFO("START");
	if(flag2 == 2)
	{
		std::ofstream myfile;
		myfile.open("/dev/rfcomm0", std::ofstream::out | std::ofstream::trunc);
		myfile << "2";
		flag2 = 1;		
		myfile.close();
		std::ofstream data;
		data.open("/home/mondiegus/catkin/src/data.txt", std::ofstream::out | std::ofstream::trunc);
		data << "2";
		data.close();
	}else{
		if(flag2 == 1)
		{
		std::ofstream myfile;
		myfile.open("/dev/rfcomm0", std::ofstream::out | std::ofstream::trunc);
		myfile << "4";
		flag2 = 2;
		myfile.close();
		std::ofstream data;
		data.open("/home/mondiegus/catkin/src/data.txt", std::ofstream::out | std::ofstream::trunc);
		data << "4";
		data.close();
		} 
	} 
  }		

private:
  ros::NodeHandle n_;
  ros::Publisher pub_;
  ros::ServiceClient client_;
  ros::Timer timer_;
  bool first_time_;
} ;

}

using namespace laser_assembler ;

int main(int argc, char **argv)
{
  ros::init(argc, argv, "periodic_snapshotter");
  ros::NodeHandle n;
  ROS_INFO("Waiting for [build_cloud] to be advertised");
  ros::service::waitForService("build_cloud");
  ROS_INFO("Found build_cloud! Starting the snapshotter");
  PeriodicSnapshotter snapshotter;
  ros::spin();
  return 0;
}
