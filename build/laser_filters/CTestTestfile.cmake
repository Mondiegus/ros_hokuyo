# CMake generated Testfile for 
# Source directory: /home/mondiegus/catkin/src/laser_filters
# Build directory: /home/mondiegus/catkin/build/laser_filters
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
ADD_TEST(_ctest_laser_filters_rostest_test_test_scan_filter_chain.launch "/home/mondiegus/catkin/build/catkin_generated/env_cached.sh" "/usr/bin/python" "/opt/ros/jade/share/catkin/cmake/test/run_tests.py" "/home/mondiegus/catkin/build/test_results/laser_filters/rostest-test_test_scan_filter_chain.xml" "--return-code" "/opt/ros/jade/share/rostest/cmake/../../../bin/rostest --pkgdir=/home/mondiegus/catkin/src/laser_filters --package=laser_filters --results-filename test_test_scan_filter_chain.xml --results-base-dir \"/home/mondiegus/catkin/build/test_results\" /home/mondiegus/catkin/src/laser_filters/test/test_scan_filter_chain.launch ")
