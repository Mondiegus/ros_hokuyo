# CMake generated Testfile for 
# Source directory: /home/mondiegus/catkin/src
# Build directory: /home/mondiegus/catkin/build
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
SUBDIRS(gtest)
SUBDIRS(rplidar_ros-slam)
SUBDIRS(laser_geometry)
SUBDIRS(laser_assembler)
SUBDIRS(laser_filters)
