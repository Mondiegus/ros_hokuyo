# generated from catkin/cmake/template/pkg.context.pc.in
CATKIN_PACKAGE_PREFIX = ""
PROJECT_PKG_CONFIG_INCLUDE_DIRS = "/home/mondiegus/catkin/install/include;/usr/include;/usr/include/eigen3".split(';') if "/home/mondiegus/catkin/install/include;/usr/include;/usr/include/eigen3" != "" else []
PROJECT_CATKIN_DEPENDS = "".replace(';', ' ')
PKG_CONFIG_LIBRARIES_WITH_PREFIX = "-llaser_geometry;-l:/usr/lib/i386-linux-gnu/libboost_system.so;-l:/usr/lib/i386-linux-gnu/libboost_thread.so;-l:/usr/lib/i386-linux-gnu/libpthread.so".split(';') if "-llaser_geometry;-l:/usr/lib/i386-linux-gnu/libboost_system.so;-l:/usr/lib/i386-linux-gnu/libboost_thread.so;-l:/usr/lib/i386-linux-gnu/libpthread.so" != "" else []
PROJECT_NAME = "laser_geometry"
PROJECT_SPACE_DIR = "/home/mondiegus/catkin/install"
PROJECT_VERSION = "1.6.4"
