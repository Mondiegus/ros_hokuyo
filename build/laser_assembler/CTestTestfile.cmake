# CMake generated Testfile for 
# Source directory: /home/mondiegus/catkin/src/laser_assembler
# Build directory: /home/mondiegus/catkin/build/laser_assembler
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
ADD_TEST(_ctest_laser_assembler_rostest_test_test_laser_assembler.launch "/home/mondiegus/catkin/build/catkin_generated/env_cached.sh" "/usr/bin/python" "/opt/ros/jade/share/catkin/cmake/test/run_tests.py" "/home/mondiegus/catkin/build/test_results/laser_assembler/rostest-test_test_laser_assembler.xml" "--return-code" "/opt/ros/jade/share/rostest/cmake/../../../bin/rostest --pkgdir=/home/mondiegus/catkin/src/laser_assembler --package=laser_assembler --results-filename test_test_laser_assembler.xml --results-base-dir \"/home/mondiegus/catkin/build/test_results\" /home/mondiegus/catkin/src/laser_assembler/test/test_laser_assembler.launch ")
